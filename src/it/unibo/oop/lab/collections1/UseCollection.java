package it.unibo.oop.lab.collections1;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * Example class using {@link List} and {@link Map}.
 * 
 */
public final class UseCollection {
	
	private static final int ELEM=100_000;
	private static final int TO_MS = 1_000_000;

    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    public static void main(final String... s) {
        /*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
        /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
        /*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
        /*
         * 8) Compute the population of the world
         */
    	
    	ArrayList<Integer> array= new ArrayList<Integer>();
    	array.ensureCapacity(ELEM);
    	populateSet(array, 1000, 2000);
    	LinkedList<Integer> linked= new LinkedList<>(array);
    	Map<String,Long> world= new TreeMap<>();
    	Integer temp;
    	long timeArray, timeLinked;
    	
    	temp=array.get(array.size()-1);
    	array.set(array.size()-1, array.get(0));
    	array.set(0, temp);
    	System.out.print("[");
    	for(Integer i : array) {
    		System.out.print(i+",");
    	}
    	System.out.println("]");
    	
    	timeArray=System.nanoTime();
    	for(int i=0; i<ELEM; i++)
    		array.add(0, i);
    	timeArray=System.nanoTime()-timeArray;
    	timeLinked=System.nanoTime();
    	for(int i=0; i<ELEM; i++)
    		linked.addFirst(i);
    	timeLinked=System.nanoTime()-timeLinked;
    	
    	
    	System.out.println("Inserting +"+ELEM+" them in an ArrayList took \t" + timeArray
                + "ns (" + timeArray / TO_MS + "ms)");
    	System.out.println("Inserting +"+ELEM+" them in an LinkedList took \t" + timeLinked
                + "ns (" + timeLinked / TO_MS + "ms)");
    	
    	world.put("Africa", 1110635000L);
    	world.put("Americas", 972005000L);
    	world.put("Antarctica", 0L);
    	world.put("Asia", 4298723000L);
    	world.put("Europe", 742452000L);
    	world.put("Oceania", 38304000L);
    	Long popOfTheWorld=0L;
    	for(Long l : world.values()) {
    		popOfTheWorld+=l;
    	}
    	System.out.println("The world contains "+popOfTheWorld+" people");
    }
    
    static void populateSet(List<Integer> list, int start, int end)
    {
    	int i=0;
    	while(i<end-start) {
    		list.add(i++);
    	}
    }
}
